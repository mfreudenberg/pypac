import sys
import yaml
import getopt
from os import path, environ
from DnfManager import DnfManager
from PacmanManager import PacmanManager
from FlatpakManager import FlatpakManager
from YayManager import YayManager
from PackageManager import PackageManager

try:
    configfile = path.abspath(path.abspath(path.dirname(__file__)) + "/pypac.yaml")
    configfile = path.expandvars(configfile)
    print("reading config file from %s"%configfile)
    with open(configfile, 'r') as configfilecontent:
        config = yaml.safe_load(configfilecontent)
except Exception as error:
    print("Something went wrong while trying to load the config file. The error was %s"%error)
    exit(1)

opts, argv = getopt.getopt(sys.argv[1:], "p:f:y:s:d:S:r")
returncode = 0
for opt, package in opts:
    packageManager = None
    returncode = 0
    if opt == "-d":
        packageManager = DnfManager(config['dnf']['packageList'])
    if opt == "-p":
        packageManager = PacmanManager(config['pacman']['packageList'])
    if opt == "-f":
        packageManager = FlatpakManager(config['flatpak']['packageList'])
    if opt == "-y":
        packageManager = YayManager(config['yay']['packageList'])
    
    if packageManager is not None:
        returncode = packageManager.install(package)
    
    if opt == "-r":
        if "pacman" in config:
            packageManager = PacmanManager(config['pacman']['packageList'])
            returncode += packageManager.restore()
            print("Finished restoring of pacman packages")
        if "dnf" in config:
            packageManager = DnfManager(config['dnf']['packageList'])
            returncode += packageManager.restore()
            print("Finished restoring of dnf packages")
        if "flatpak" in config:
            packageManager = FlatpakManager(config['flatpak']['packageList'])
            returncode += packageManager.restore()
            print("Finished restoring of flatpak packages")
        if "yay" in config:
            packageManager = YayManager(config['yay']['packageList'])
            returncode += packageManager.restore()
            print("Finished restoring of yay packages")

    if opt == "-s":
        if "pacman" in config:
            print("searching with pacman:")
            pacman = PacmanManager(config['pacman']['packageList'])
            returncode += pacman.search(package)
        if "dnf" in config:
            print("searching with dnf:")
            dnf = DnfManager(config['dnf']['packageList'])
            returncode += dnf.search(package)
        if "flatpak" in config:
            print("searching with flatpak:")
            flatpak = FlatpakManager(config['flatpak']['packageList'])
            returncode += flatpak.search(package)
        if "yay" in config:
            print("searching with yay:")
            yay = YayManager(config['yay']['packageList'])
            returncode += yay.search(package)
    if opt == "-S":
        # setup repositories
        packageManager = PackageManager()
        for key in config:
            packageManager.setupRepositories(config[key])


if returncode != 0:
    print("Something went wrong! The return code is " + str(returncode))
        