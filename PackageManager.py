from os.path import exists
from os import linesep, path, system
import json

class PackageManager():

    def __init__(self, packagelistfile):
        self.packagelistfile = path.expandvars(packagelistfile)

    def install(self, package):
        pass

    def search(self, package):
        pass

    def restore(self):
        print("restoring all packages")
        for package in self.getPackages():
            return self.install(package)
    
    def getPackages(self):
        with open(self.packagelistfile, 'r') as packagefile:
            packages = json.load(packagefile)
            sanitizes_packages = [ p.strip() for p in packages ]
            return sanitizes_packages

    def addPackage(self, package):
        packages = self.getPackages()
        with open(self.packagelistfile, 'w') as packagefile:
            if package not in packages:
                packages.append(package)
                json.dump(packages, packagefile)

    def setupRepositories(self, config):
        for line in config['additionalRepositoriesScript']:
            system(line)
