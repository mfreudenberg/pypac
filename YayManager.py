from os import system
from PackageManager import PackageManager

class YayManager(PackageManager):
    def install(self, package):
        returncode = system("yay -S --noconfirm " + package)
        if returncode == 0:
            self.addPackage(package)
        return returncode

    def search(self, package):
        returncode = system("yay -Ss " + package)
        return returncode

    def restore(self, package):
        returncode = system("yay -S --noconfirm " + package)
        return returncode