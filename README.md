# Welcome!

This is pypac, my little **py**thon **pac**acke manager for linux. As this tool is written in python, it even might work on Windows based systems (with tools like `choco` or `winget`)! Of course i only have tested it on linux-based systems (fedora and some arch systems).

As this is a python learning project, i might be in a constant WIP-state. 

## Features
Pypac is a small helper, that i'd like to use to *search*, *install* and *restore* packages on my linux machine(s). As i often like to distro-hop i was in the need to have a small tool, that works on (most) linux-desktops and allows me to install the basic packages from scratch. 

The idea is, to save all installed packes in a small json-file, for each package manager. Thus you can store and backup your personal packages references somewhere else and reuse them when you do your next distro-hop. 

# WIP-Area

I'm currently working on the setup area, that allows me to do some initial setup of repositories. In case i have some packages in my pypac.json, that are not in the 