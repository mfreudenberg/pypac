from os import system
from PackageManager import PackageManager

class PacmanManager(PackageManager):
    def install(self, package):
        returncode = system("sudo pacman -S --noconfirm " + package)
        if returncode == 0:
            self.addPackage(package)
        return returncode

    def search(self, package):
        returncode = system("pacman -Ss " + package)
        return returncode