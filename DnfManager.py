from os import system
from PackageManager import PackageManager

class DnfManager(PackageManager):
    def install(self, package):
        returncode = system("sudo dnf install " + package)
        if returncode == 0:
            self.addPackage(package)
        return returncode

    def search(self, package):
        returncode = system("dnf search " + package)
        return returncode

    def restore(self):
        packages = self.getPackages()
        packages_str = " ".join(p for p in packages)
        returncode = system("sudo dnf install " + packages_str)
        return returncode